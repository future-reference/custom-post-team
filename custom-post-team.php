<?php

/**
 * @wordpress-plugin
 * Plugin Name:       Custom Post Type - Team
 * Plugin URI:        https://gitlab.com/future-reference/custom-post-team
 * Description:       Wordpress Plugin to create a custom post type named team
 * Version:           1.0.0
 * Author:            Future Reference
 * Author URI:        http://futurereference.co/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       custom-post-team
 */

add_action('init', 'team_register_post_types');

function team_register_post_types()
{
    /* Register the Team post type. */
    register_post_type(
        'team',
        array(
            'description'         => '',
            'public'              => true,
            'publicly_queryable'  => true,
            'show_in_nav_menus'   => false,
            'show_in_admin_bar'   => true,
            'exclude_from_search' => false,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'menu_position'       => 20,
            'menu_icon'           => 'dashicons-groups',
            'can_export'          => true,
            'delete_with_user'    => false,
            'hierarchical'        => false,
            'has_archive'         => false,
            'capability_type'     => 'post',

            /* The rewrite handles the URL structure. */
            'rewrite' => array(
                'slug'       => 'team',
                'with_front' => false,
                'pages'      => true,
                'feeds'      => true,
                'ep_mask'    => EP_PERMALINK,
            ),

            /* What features the post type supports. */
            'supports' => array(
                'title',
                'editor',
                'revisions',
                'thumbnail'
            ),

            /* Labels used when displaying the posts. */
            'labels' => array(
                'name'               => __('Teams',                         'futurereference'),
                'singular_name'      => __('Team',                          'futurereference'),
                'menu_name'          => __('Teams',                         'futurereference'),
                'name_admin_bar'     => __('Teams',                         'futurereference'),
                'add_new'            => __('Add New Member',                'futurereference'),
                'add_new_item'       => __('Add New Team Member',           'futurereference'),
                'edit_item'          => __('Edit Team Member',              'futurereference'),
                'new_item'           => __('New Team Member',               'futurereference'),
                'view_item'          => __('View Team Member',              'futurereference'),
                'search_items'       => __('Search Teams Members',          'futurereference'),
                'not_found'          => __('No Team found',                 'futurereference'),
                'not_found_in_trash' => __('No Team Member found in trash', 'futurereference'),
                'all_items'          => __('Team Members',                  'futurereference'),
            )
        )
    );
}
